# VHDL Code Auto-Formatter for VSCode

## Setup

Clone this repository and open the workspace in VSCode:

```bash
git clone git@gitlab.com:detroitenglish/vscode-settings-vhdl-plth.git

cd vscode-settings-vhdl-plth

code .
```

VSCode will prompt you to install the recommended extensions when the workspace
is opened for the first time... install these extensions.

You can also find the required extensions by searching for `@recommended`:

![](.gitlab/recommended-extensions.png)

## Configuration

You can change the code formatting options by editing the
`.vscode/settings.json` file.

To see the new formatting options applied, simply open `test.vhdl` and save the
file. The new code format options will be applied on save.

## Applying to other VHDL projects

Agree upon the code formatting options to be made standard for your team.

Once standardized, simply copy and paste the `.vscode` folder into the root of
any VHDL project directory 😎
